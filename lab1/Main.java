import org.jacop.core.*;
import org.jacop.constraints.*;
import org.jacop.search.*;

public class Main {
    public static void main (String[] args) {


        // int n = 9;
        // int n_prefs = 17;
        // int[][] prefs = {{1,3}, {1,5}, {1,8}, {2,5}, {2,9}, {3,4}, {3,5}, {4,1},
        // {4,5}, {5,6}, {5,1}, {6,1}, {6,9}, {7,3}, {7,8}, {8,9}, {8,7}};


        int n = 11;
        int n_prefs = 20;
        int[][] prefs = {{1,3}, {1,5}, {2,5}, {2,8}, {2,9}, {3,4}, {3,5}, {4,1},
        {4,5}, {4,6}, {5,1}, {6,1}, {6,9}, {7,3}, {7,5}, {8,9}, {8,7}, {8,10},
        {9, 11}, {10, 11}};


        // int n = 15;
        // int n_prefs = 20;
        // int[][] prefs = {{1,3}, {1,5}, {2,5}, {2,8}, {2,9}, {3,4}, {3,5}, {4,1},
        // {4,15}, {4,13}, {5,1}, {6,10}, {6,9}, {7,3}, {7,5}, {8,9}, {8,7}, {8,14},
        // {9, 13}, {10, 11}};




        System.out.print("\n\n");
        System.out.print("\n---------------------------------------------------------------------------------\n");
        int r1 = method1(n,  n_prefs,  prefs);
        System.out.print("\n\n");
        System.out.print("\n---------------------------------------------------------------------------------\n");
        int r2 =  method2(n,  n_prefs,  prefs);
        System.out.print("\n\n");
        System.out.print("\n---------------------------------------------------------------------------------\n");
        System.out.print("result =" + r1 + "("+ r2 + ")");
        System.out.print("\n---------------------------------------------------------------------------------\n");
        System.out.print("\n\n");
    }



    // System.out.print("\n" + i + "\n");
    // System.out.print("\t" + pref[0] + "\n");
    // System.out.print("\t" + pref[1] + "\n");
    public static int method1(int n, int n_prefs, int[][] prefs){
        Store store = new Store(); // define store

        IntVar[] RowIndexes = new IntVar[n];
        for (int i = 0; i < n; i++){
            RowIndexes[i] = new IntVar(store, "p"+Integer.toString(i), 1, n);
        }
        store.impose(new Alldiff(RowIndexes));

        // räkna ut varje enskild cost
        IntVar[] costOnPref = new IntVar[n_prefs];

        for (int i = 0; i < n_prefs; i++){

            costOnPref[i] = new IntVar(store, 0, 1);

            int[] pref = prefs[i];

            IntVar thisItem = RowIndexes[pref[0]-1]  ;
            IntVar thatItem = RowIndexes[pref[1]-1] ;


            // calculate acctual distance
            IntVar dist = new IntVar(store, 0, n);
            Constraint d = new Distance(thisItem, thatItem, dist);
            store.impose(d);

            // if distance is greater than 1, it costs 1 point
            Constraint re = new Reified( new XgtC(dist, 1), costOnPref[i]);
            store.impose(re);

        }


        // summera cost
        IntVar cost = new IntVar(store, "cost", 0, n_prefs);
        Constraint CostConstr = new Sum(costOnPref, cost);
        store.impose(CostConstr);


        Search<IntVar> search = new DepthFirstSearch<IntVar>();


        SelectChoicePoint<IntVar> select = new SimpleSelect<IntVar>(
        RowIndexes, new SmallestDomain<IntVar>(), new IndomainMin<IntVar>());

        // search.setSolutionListener(new PrintOutListener<IntVar>());
        // search.labeling(store, select, cost);

        if(search.labeling(store, select, cost)) {
            System.out.print("OUTPUT: " + (prefs.length - cost.value())+ "\n\n");

        } else {
            System.out.print("\nNo Solution\n");
        }
        return (prefs.length - cost.value());
    }










    public static int method2(int n, int n_prefs, int[][] prefs){
        Store store = new Store(); // define store

        IntVar[] RowIndexes = new IntVar[n];
        for (int i = 0; i < n; i++){
            RowIndexes[i] = new IntVar(store, "p"+Integer.toString(i), 1, n);
        }
        store.impose(new Alldiff(RowIndexes));

        // räkna ut varje enskild cost
        IntVar[] costOnPref = new IntVar[n_prefs];

        for (int i = 0; i < n_prefs; i++){

            costOnPref[i] = new IntVar(store, 0, n);

            int[] pref = prefs[i];

            IntVar thisItem = RowIndexes[pref[0]-1]  ;
            IntVar thatItem = RowIndexes[pref[1]-1] ;


            // calculate acctual distance
            IntVar dist = new IntVar(store, 0, n);
            Constraint d = new Distance(thisItem, thatItem, costOnPref[i]);
            store.impose(d);

        }


        // summera cost
        IntVar cost = new IntVar(store, "cost", 0, n);
        Constraint CostConstr = new Max(costOnPref, cost);
        store.impose(CostConstr);


        Search<IntVar> search = new DepthFirstSearch<IntVar>();


        SelectChoicePoint<IntVar> select = new SimpleSelect<IntVar>(
        RowIndexes, new SmallestDomain<IntVar>(), new IndomainMin<IntVar>());

        // search.setSolutionListener(new PrintOutListener<IntVar>());
        // search.labeling(store, select, cost);
        if(search.labeling(store, select, cost)) {
            System.out.print("OUTPUT: " + cost.value()+ "\n\n");

        } else {
            System.out.print("\nNope\n");
        }
        return cost.value();

    }


}
