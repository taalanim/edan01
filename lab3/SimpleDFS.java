/**
*  SimpleDFS.java
*  This file is part of JaCoP.
*
*  JaCoP is a Java Constraint Programming solver.
*
*	Copyright (C) 2000-2008 Krzysztof Kuchcinski and Radoslaw Szymanek
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU Affero General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU Affero General Public License for more details.
*
*  Notwithstanding any other provision of this License, the copyright
*  owners of this work supplement the terms of this License with terms
*  prohibiting misrepresentation of the origin of this work and requiring
*  that modified versions of this work be marked in reasonable ways as
*  different from the original version. This supplement of the license
*  terms is in accordance with Section 7 of GNU Affero General Public
*  License version 3.
*
*  You should have received a copy of the GNU Affero General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*/

// import org.jacop.constraints.Not;
// import org.jacop.constraints.PrimitiveConstraint;
// import org.jacop.constraints.XeqC;
import org.jacop.constraints.*;
// import org.jacop.core.FailException;
// import org.jacop.core.IntDomain;
// import org.jacop.core.IntVar;
// import org.jacop.core.Store;
import org.jacop.core.*;

/**
* Implements Simple Depth First Search .
*
* @author Krzysztof Kuchcinski
* @version 4.1
*/


public class SimpleDFS  {

    int backtrack = 0;
    int best = 0;
    int nodes = 0;

    // these 2 are modified by the makefile and controls what version of algorithm are used
    int version = 1;
    int selectVersion = 2;

    boolean trace = false;

    /**
    * Store used in search
    */
    Store store;

    /**
    * Defines varibales to be printed when solution is found
    */
    IntVar[] variablesToReport;

    /**
    * It represents current depth of store used in search.
    */
    int depth = 0;

    /**
    * It represents the cost value of currently best solution for FloatVar cost.
    */
    public int costValue = IntDomain.MaxInt;

    /**
    * It represents the cost variable.
    */
    public IntVar costVariable = null;

    public SimpleDFS(Store s) {
        store = s;
    }


    /**
    * This function is called recursively to assign variables one by one.
    */
    public void pr(IntVar[] vars) {
        if (trace) {
            for (int i = 0; i < vars.length; i++)
            System.out.println (vars[i] + "  size = " + vars[i].dom().getSize() );
            System.out.println ();
        }

    }
    public boolean label(IntVar[] vars) {

        System.out.println ("Value version = " + version + "\nVariable Version = " + selectVersion);
        //System.out.println ("Syntax : ");
        //System.out.println ("variable = { min : max : nbr of options : choice }\n\n");
        if (trace){
            System.out.println ("\noriginal");
        }
        pr(vars);
        System.out.println ("--------------------------------------------------------------------------\n\n\n");
        boolean result = label2(vars);
        System.out.println ("--------------------------------------------------------------------------\n\n\n");
        System.out.println ("( Nodes : wrongs : best found at time )");
        System.out.println ("(" + nodes+" : " + backtrack + " : " + best + ")\n");
        return result;
    }

    public boolean label2(IntVar[] vars) {

            nodes ++;


        ChoicePoint choice = null;
        boolean consistent;

        // Instead of imposing constraint just restrict bounds
        // -1 since costValue is the cost of last solution
        if (costVariable != null) {
            try {
                if (costVariable.min() <= costValue - 1)
                costVariable.domain.in(store.level, costVariable, costVariable.min(), costValue - 1);
                else
                return false;
            } catch (FailException f) {
                return false;
            }
        }

        pr(vars);

        if (trace) {
            System.out.println("Consistency\n");
        }
        consistent = store.consistency();

        pr(vars);

        if (!consistent) {
            // Failed leaf of the search tree
            backtrack ++;
            return false;
        } else { // consistent

            if (vars.length == 0) {// just an error program...
                // solution found; no more variables to label2

                // update cost if minimization
                if (costVariable != null)
                costValue = costVariable.min();

                reportSolution();
                best = backtrack;

                return costVariable == null; // true is satisfiability search and false if minimization
            }

            choice = new ChoicePoint(vars);

            levelUp();

            store.impose(choice.getConstraint());

            // choice point imposed.

            consistent = label2(choice.getSearchVariables());

            // System.out.println ("\n\n\nbacktracking is " + backtrack);
            if (consistent) {
                levelDown();
                return true;
            } else {

                restoreLevel();

                store.impose(new Not(choice.getConstraint()));

                // negated choice point imposed.

                consistent = label2(vars);

                levelDown();

                if (consistent) {
                    return true;
                } else {
                    return false;
                }
            }
        }

    }

    void levelDown() {
        store.removeLevel(depth);
        store.setLevel(--depth);
    }

    void levelUp() {
        store.setLevel(++depth);
    }

    void restoreLevel() {
        store.removeLevel(depth);
        store.setLevel(store.level);
    }

    public void reportSolution() {
        if(true ){

        if (costVariable != null)
        System.out.println ("Cost is " + costVariable);
        trace = false;
        for (int i = 0; i < variablesToReport.length; i++){
            System.out.print (variablesToReport[i] + " ");
            if (i % 3 == 0 && i != 0){
                System.out.print ("\n");
            }
        }
        System.out.println ("\n---------------");
    }
    }

    public void setVariablesToReport(IntVar[] v) {
        variablesToReport = v;
    }

    public void setCostVariable(IntVar v) {
        costVariable = v;
    }

    public class ChoicePoint {
        IntVar var;
        IntVar[] searchVariables;
        int value;

        public ChoicePoint (IntVar[] v) {
            var = selectVariable(v);
            value = selectValue(var);
        }

        public IntVar[] getSearchVariables() {
            return searchVariables;
        }


        //-------------------------------------------------------------------------------------
        IntVar originalSelect(IntVar[] v) {
            if(version == 0 || v[0].getSize() == 1){
                searchVariables = new IntVar[v.length-1];
                for (int i = 0; i < v.length-1; i++) {
                    searchVariables[i] = v[i+1];
                }
            } else {
                searchVariables = new IntVar[v.length];
                for (int i = 0; i < v.length; i++) {
                    searchVariables[i] = v[i];
                }
            }
            return v[0];
        }



        IntVar smartSelect1(IntVar[] v) {
            int index = 0;
            for (int i = 0 ; i < v.length; i++){
                if (v[i].getSize() < v[index].getSize()){
                    index = i;
                }
            }

            if(version == 0 || v[index].getSize() == 1){
                searchVariables = new IntVar[v.length-1];
                //do smarter
                int distancer = 0;
                for (int i = 0; i < v.length-1; i++) {
                    if (i == index){
                        distancer++;
                    }
                    searchVariables[i] = v[i + distancer];
                }


            } else {
                searchVariables = new IntVar[v.length];
                for (int i = 0; i < v.length; i++) {
                    searchVariables[i] = v[i];
                }
            }
            return v[index];
        }



        IntVar smartSelect2(IntVar[] v) {
            int index = 0;
            for (int i = 0 ; i < v.length; i++){
                if (v[i].getSize() > v[index].getSize()){
                    index = i;
                }
            }

            if(version == 0 || v[index].getSize() == 1){
                searchVariables = new IntVar[v.length-1];
                //do smarter
                int distancer = 0;
                for (int i = 0; i < v.length-1; i++) {
                    if (i == index){
                        distancer++;
                    }
                    searchVariables[i] = v[i + distancer];
                }

            } else {
                searchVariables = new IntVar[v.length];
                for (int i = 0; i < v.length; i++) {
                    searchVariables[i] = v[i];
                }
            }
            return v[index];
        }
        //-------------------------------------------------------------------------------------


        /**
        * example variable selection; input order
        */
        IntVar selectVariable(IntVar[] v) {
            if (v.length != 0) {
                switch(selectVersion){
                    case 0:
                    return originalSelect(v);
                    // break;
                    case 1:
                    return smartSelect1(v);
                    // break;
                    case 2:
                    return smartSelect2(v);
                    // break;
                }
            }
            else {
                System.err.println("Zero length list of variables for label2ing");
                return new IntVar(store);
            }
            System.out.print ("ERROR");
            System.err.println("ERROR");
            return originalSelect(v);
        }


        /**
        * example value selection; indomain_min
        */
        int selectValue(IntVar v) {
            int choice = -1;
            switch(version){
                case 0:
                    choice = v.min();
                    break;
                case 1:
                    choice = (v.max()+v.min())/2;;
                    break;
                case 2:
                    // choice = (v.max()+v.min())/2;;
                    int a = v.max()+v.min();
                    if ( a%2==0 ){
                        choice = a/2;;
                    } else {
                        choice = ++a/2;;
                    }
                    break;
            }
            if (trace){
                System.out.print ("Options\n" + v + "\nchoice =" + choice +"\n\n\n");
            }
            return choice;
        }

        /**
        * example constraint assigning a selected value
        */
        public PrimitiveConstraint getConstraint() {
            switch (version) {
                case 0:
                return new XeqC(var, value);
                // break;
                case 1:
                return new XlteqC(var, value);
                // break;
                case 2:
                return new XgteqC(var, value);
                // break;
            }
            System.out.print ("ERROR");
            System.err.println("ERROR");
            return new XeqC(var, value);
        }
    }
}
