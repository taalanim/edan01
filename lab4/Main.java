import org.jacop.core.*;
import org.jacop.constraints.*;
import org.jacop.search.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static int del_add = 1;
    public static int del_mul = 2;

    public static int n = 28;

    public static int[] last = {27,28};

    public static int[] add = {9,10,11,12,13,14,19,20,25,26,27,28};

    public static int[] mul = {1,2,3,4,5,6,7,8,15,16,17,18,21,22,23,24};

    public static final String FILENAME1 = "./log1.txt";
    public static final String FILENAME2 = "./log2.txt";
    public static final String FILENAME3 = "./log3.txt";
    public static final String FILENAME4 = "./log4.txt";
    public static final String FILENAME5 = "./log5.txt";
    public static final String FILENAME6 = "./log6.txt";

    public static BufferedWriter bw;
    public static FileWriter fw;

    public static int[][] dependencies = {
        /*1*/  {9},
        /*2*/  {9},
        /*3*/  {10},
        /*4*/  {10},
        /*5*/  {11},
        /*6*/  {11},
        /*7*/  {12},
        /*8*/  {12},
        /*9*/  {27},
        /*10*/ {28},
        /*11*/ {13},
        /*12*/ {14},
        /*13*/ {16,17},
        /*14*/ {15,18},
        /*15*/ {19},
        /*16*/ {19},
        /*17*/ {20},
        /*18*/ {20},
        /*19*/ {22,23},
        /*20*/ {21,24},
        /*21*/ {25},
        /*22*/ {25},
        /*23*/ {26},
        /*24*/ {26},
        /*25*/ {27},
        /*26*/ {28},
        /*27*/ {} ,
        /*28*/ {},
    };

    public static void main (String[] args) {
        int r1 = 0;
        int r2 = 0;
        int r3 = 0;
        int r4 = 0;
        int r5 = 0;
        int r6 = 0;
        System.out.print("\n-------working on number 1---------------------------------------------------------\n\n\n");
        r1 = smartMethod(1, 1, 1);
        System.out.print("\n-------working on number 2---------------------------------------------------------\n\n\n");
        r2 = smartMethod(2, 1, 2);
        System.out.print("\n-------working on number 3---------------------------------------------------------\n\n\n");
        r3 = smartMethod(3, 1, 3);
        System.out.print("\n-------working on number 4---------------------------------------------------------\n\n\n");
        r4 = smartMethod(4, 2, 2);
        System.out.print("\n-------working on number 5---------------------------------------------------------\n\n\n");
        r5 = smartMethod(5, 2, 3);
        System.out.print("\n-------working on number 6---------------------------------------------------------\n\n\n");
        r6 = smartMethod(6, 2, 4);
        System.out.print("( " + r1 + " : " + r2 +  " : " + r3 +" : " + r4  + " : " + r5 +" : " + r6 + " )\n");
        System.out.print("\n----------------------------------------------------------------\n");
    }

    public static int smartMethod(int filenumber , int add_limit, int mul_limit){

        try {
            fw = new FileWriter("./log" + filenumber  + ".txt");
            bw = new BufferedWriter(fw);
            bw.write("log " + filenumber + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

        int r = method( add_limit,  mul_limit);
        try {

            if (bw != null)
            bw.close();
            if (fw != null)
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return r;
    }

    //http://fileadmin.cs.lth.se/cs/Education/EDAN01/presentations/lecture5.pdf
    public static int method(int add_limit, int mul_limit){
        Store store = new Store(); // define store
        //syntax
        int max_time;
        int max_delay;
        if(del_add > del_mul){
            max_delay = del_add;
        } else {
            max_delay = del_mul;
        }
        max_time = max_delay * n;

        // du[i] = duration of node[i]
        IntVar[] du = new IntVar[n];
        for (int i = 0; i < add.length; i++) {
            du[ add[i] -1] = new IntVar(store, "du" + (add[i]), del_add, del_add);
        }
        for (int i = 0; i < mul.length; i++) {
            du[ mul[i] -1] = new IntVar(store, "du" + (mul[i]), del_mul, del_mul);
        }

        // init start and end times
        IntVar[] st = new IntVar[n];
        IntVar[] et = new IntVar[n];
        IntVar[] syn = new IntVar[n];
        for (int i = 0; i < n; i++) {
            st[i] = new IntVar(store, "st" + (i+1), 0, max_time);
            et[i] = new IntVar(store, "et" + (i+1), 0, max_time + max_delay);
            // store.impose( new XplusYeqZ( st[i], du[i]-1, et[i]));
            syn[i] = new IntVar(store, "syn" + (i+1), 0, max_delay);
            store.impose( new XplusCeqZ( du[i], -1,  syn[i]));
            store.impose( new XplusYeqZ( st[i], syn[i], et[i]));
        }

        //implement dependencies
        for (int i = 0; i < dependencies.length; i++){
            for (int j = 0; j < dependencies[i].length; j++){
                store.impose( new XgtY( st[dependencies[i][j] -1], et[i] ) );
            }
        }

        // make separate copies as to make syntax simpler
        IntVar[] add_st = new IntVar[add.length];
        IntVar[] add_du = new IntVar[add.length];
        IntVar[] add_syntax_ones = new IntVar[add.length];
        for (int i = 0; i < add.length; i ++ ){
            add_st[i] = new IntVar(store,"", 0, max_time);
            add_du[i] = new IntVar(store, "add_du" + (i+1) , 0, max_time + del_add);
            store.impose( new XeqY( st[ add[i] -1] , add_st[i] ));
            store.impose( new XeqY( du[ add[i] -1] , add_du[i] ));
            add_syntax_ones[i] = new IntVar(store, "1", 1, 1);
        }
        IntVar[] mul_st = new IntVar[mul.length];
        IntVar[] mul_du = new IntVar[mul.length];
        IntVar[] mul_syntax_ones = new IntVar[mul.length];
        for (int i = 0; i < mul.length; i ++ ){
            mul_st[i] = new IntVar(store, "", 0, max_time);
            mul_du[i] = new IntVar(store, "mul_du" + (i+1) , 0, max_time + del_mul);
            store.impose( new XeqY( st[ mul[i] -1] , mul_st[i] ));
            store.impose( new XeqY( du[ mul[i] -1] , mul_du[i] ));
            mul_syntax_ones[i] = new IntVar(store, "1", 1, 1);
        }



        // make cumuative ------------------------------------------------------------------------------------------------
        store.impose( new Cumulative( add_st, add_du, add_syntax_ones, new IntVar(store, "add_limit", add_limit, add_limit)));
        store.impose( new Cumulative( mul_st, mul_du, mul_syntax_ones, new IntVar(store, "mul_limit", mul_limit, mul_limit)));




        // make Diff2 ------------------------------------------------------------------------------------------------
        IntVar[] add_origin2 = new IntVar[add.length];
        for (int i = 0; i < add.length; i ++ ){
            add_origin2[i] = new IntVar(store, "add_o2" + (i+1), 0, add_limit );
        }
        IntVar[] mul_origin2 = new IntVar[mul.length];
        for (int i = 0; i < mul.length; i ++ ){
            mul_origin2[i] = new IntVar(store, "mul_o2" + (i+1), 0, mul_limit );
        }
        store.impose( new Diff2( add_st, add_origin2, add_du, add_syntax_ones));
        store.impose( new Diff2( mul_st, mul_origin2, mul_du, mul_syntax_ones));




        // cost function
        IntVar cost = new IntVar(store, "cost", 0, max_time + max_delay);
        store.impose( new Max( et , cost));

        IntVar[] XXX = new IntVar[n];
        for (int i = 0; i < add.length; i++){
            XXX[i] = add_origin2[i];
        }


        for (int i = 0; i < mul.length; i++){
            XXX[i + add.length] = mul_origin2[i];
        }



        Search<IntVar> slaveSearch = new DepthFirstSearch<IntVar>();

        SelectChoicePoint<IntVar> slaveSelect = new SimpleSelect<IntVar>( XXX, new SmallestDomain<IntVar>(), new IndomainMin());

        slaveSearch.setSelectChoicePoint(slaveSelect);

        Search<IntVar> mainSearch = new DepthFirstSearch<IntVar>();

        SelectChoicePoint<IntVar> mainSelect = new SimpleSelect<IntVar>(st, new SmallestDomain<IntVar>(), new IndomainMin<IntVar>());

        mainSearch.addChildSearch(slaveSearch);

        //mainSearch.setSolutionListener(new PrintOutListener<IntVar>());
        long t1 = System.currentTimeMillis();
        mainSearch.labeling(store, mainSelect, cost);
        long t2 = System.currentTimeMillis();



        // name = add[i]
        // start = add_st[i]
        // end = add_et[i]
        // procc = add_origin2[i]
        try {
            int iCost = cost.min();
            bw.write("add_limit = " + add_limit + "\n");
            bw.write("mul_limit = " + mul_limit + "\n");
            bw.write("OUTPUT: cost = " + iCost + "\n");
            bw.write("time = " + (t2-t1) + "\n\n");
            //     for (int i = 0; i < n; i++ ){
            //         bw.write(st[i] + "" );
            //         if (i < 9){bw.write(" ");}
            //         if (st[i].min() < 10){bw.write(" ");}
            //         bw.write("   :   " + et[i]  );
            //         if (i < 9){bw.write(" ");}
            //         if (et[i].min() < 10){bw.write(" ");}
            //         bw.write("   :   " + du[i]  );
            //         if (du[i].min() < 10){bw.write(" ");}
            //         bw.write( "\n" );

            //   }

            // bw.write("\n-------------------------------------------\n" );
            //
            // for (int k = 0; k < add_limit; k++){
            //     bw.write("\nUnit-add-" + (k+1) + " \n" );
            //     //initiate
            //
            //     // fill with acctual data
            //     int nbr = 0;
            //     for (int i = 0; i < add.length; i++){
            //         if(add_origin2[i].min() == k){
            //             if(nbr % 4 == 0 ){
            //                 bw.write("\n");
            //             }
            //             nbr++;
            //             bw.write("node " +add[i] + ", ");
            //             if (add[i] < 10){bw.write(" ");}
            //             bw.write("start" + add_st[i] + "\n" );
            //         }
            //     }
            //     bw.write("\n-------------------------------------------\n" );
            // }
            // for (int k = 0; k < mul_limit; k++){
            //     bw.write("\nUnit-mul-" + (k+1) + " \n" );
            //
            //     // fill with acctual data
            //     int nbr = 0;
            //     for (int i = 0; i < mul.length; i++){
            //         if(mul_origin2[i].min() == k){
            //             if(nbr % 4 == 0 ){
            //                 bw.write("\n");
            //             }
            //             nbr++;
            //             bw.write("node " +mul[i] + ", ");
            //             if (mul[i] < 10){bw.write(" ");}
            //             bw.write("start" + mul_st[i] + "\n" );
            //         }
            //     }
            //     bw.write("\n-------------------------------------------\n" );
            // }







            int[][] timeline = new int[add_limit + mul_limit][iCost+1];

            //  bw.write("\n-------------------------------------------\n" );

            for (int k = 0; k < add_limit; k++){
                // bw.write("\nUnit-add-" + (k+1) + " \n" );
                //initiate
                for (int i = 0; i <= iCost; i++){
                    timeline[k][i] = 0;
                }
                // fill with acctual data
                for (int i = 0; i < add.length; i++){
                    if(add_origin2[i].min() == k){
                        timeline[k][add_st[i].min()] = add[i];
                    }
                }
            }
            for (int k = 0; k < mul_limit; k++){
                // bw.write("\nUnit-mul-" + (k+1) + " \n" );
                //initiate

                for (int i = 0; i <= iCost; i++){
                    timeline[k + add_limit][i] = 0;
                }
                // fill with acctual data
                for (int i = 0; i < mul.length; i++){

                    if(mul_origin2[i].min() == k){
                        timeline[k + add_limit][mul_st[i].min()] = mul[i];
                    }
                }
            }
        bw.write("\n-------------------------------------------------------\n[t]" );
            for (int i = 0; i < add_limit; i++){
                bw.write("\t   <add-" + (i+1) + "> " );
            }
            // bw.write(" " );
            for (int i = 0; i < mul_limit; i++){
                bw.write("\t   <mul-" + (i+1) + "> " );
            }
            bw.write("\n" );




            int nbr = 0;
            for (int i = 0; i <= iCost; i++){
                if(nbr % 4 == 0 ){
                    bw.write("\n");
                }
                nbr++;

                bw.write("[" + i + "]");
                if (i < 10){bw.write(" ");}

                for (int k = 0; k < add_limit + mul_limit; k++){


                    //bw.write( k + " " + i      );
                    if(timeline[k][i] != 0){
                        bw.write("\t [");
                        bw.write(Integer.toString(timeline[k][i]));
                        bw.write("]");
                        //if (timeline[k][i] < 10){bw.write(" ");}
                        bw.write("\t");

                    } else{

                        if(k > add_limit-1 &&   timeline[k][i-1]!= 0){

                            bw.write("\t [");
                            bw.write(Integer.toString(timeline[k][i-1]));
                            bw.write("]");
                            //if (timeline[k][i] < 10){bw.write(" ");}
                            bw.write("\t");

                        } else {
                            bw.write("\t --- \t");
                        }
                    }
                }
                bw.write( "\n" );
            }

            bw.write("\n-------------------------------------------------------\n" );





        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return (cost.value());
    }




}
