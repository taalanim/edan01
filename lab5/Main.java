import org.jacop.core.*;
import org.jacop.constraints.*;
import org.jacop.search.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Main {


    public static BufferedWriter bw;
    public static FileWriter fw;

    public static void main (String[] args) {

        int r1 = 0;
        int r2 = 0;
        int r3 = 0;


        System.out.print("\n-------working on number 1---------------------------------------------------------\n\n\n");
        r1 = smartMethod(1, 5, 13, 12, new int[]{-5, -4, -3, 3, 4, 5});

        System.out.print("\n-------working on number 2---------------------------------------------------------\n\n\n");
        r2 = smartMethod(2, 5, 7, 18, new int[]{-5, -4, -3, 3, 4, 5});

        System.out.print("\n-------working on number 3---------------------------------------------------------\n\n\n");
        r3 = smartMethod(3, 7, 20, 29, new int[]{-7, -6, -5, -4, 4, 5, 6, 7});

        System.out.print("( " + r1 + " : " + r2 +  " : " + r3 + ")\n");
        System.out.print("\n----------------------------------------------------------------\n");
    }

    public static int smartMethod(int filenumber, int n, int n_commercial, int n_residential, int[] costArray ){
        try {
            fw = new FileWriter("./log" + filenumber  + ".txt");
            bw = new BufferedWriter(fw);
            bw.write("log " + filenumber + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
        int r = method( n, n_commercial, n_residential, costArray  );
        try {

            if (bw != null)
            bw.close();
            if (fw != null)
            fw.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return r;
    }


    // residential = 1
    // costArray
    /*
    0    -5
    1    -4
    2    -3
    3    +3
    4    +4
    5    +5

    0    -7
    1    -6
    2    -5
    3    -4
    4    +4
    5    +5
    6    +6
    7    +7
    */
    public static int method(int n, int n_commercial, int n_residential, int[] costArray ){

        Store store = new Store(); // define store
        int point_max = 0;
        int point_min = 0;
        for (int i = 0; i < costArray.length; i ++){
            if ( costArray[i] < point_min ){
                point_min = costArray[i];
            } else if ( costArray[i] > point_max ){
                point_max = costArray[i];
            }
        }

        // residential = 1
        IntVar[][] grid = new IntVar[n][n];
        for (int i = 0; i < n; i ++){
            for (int j = 0; j < n; j ++){
                // residential = 1
                grid[i][j] = new IntVar(store, "grid[" + i + "]" + "[" + j + "]" , 0, 1);
            }
        }


        // IntVar[] iv_costArray = new IntVar[costArray.length];
        // for (int i = 0; i < costArray.length; i ++){
        //     iv_costArray[i] = new IntVar(store, "ivca[" + i + "]" , costArray[i], costArray[i]);
        //
        // }




        // start of constraints----------------------------------------------------------------

        // count the number of residents per unit
        IntVar[] v_Number = new IntVar[n];
        IntVar[] h_Number = new IntVar[n];


        for (int i = 0; i < n; i ++){
            v_Number[i] = new IntVar(store, "v_Cost[" + i + "]" , 0, n);
            h_Number[i] = new IntVar(store, "h_Cost[" + i + "]" , 0, n);

            store.impose( new Sum( grid[i], h_Number[i]));
            store.impose( new Sum( getVertical( i, grid ), v_Number[i]));
        }



        // calculate the costs for said units
        IntVar[] v_Cost = new IntVar[n];
        IntVar[] h_Cost = new IntVar[n];
        for (int i = 0; i < n; i ++){
            v_Cost[i] = new IntVar(store, "v_Cost[" + i + "]" , point_min, point_max);
            h_Cost[i] = new IntVar(store, "h_Cost[" + i + "]" , point_min, point_max);

            store.impose( new ElementInteger(v_Number[i], costArray, v_Cost[i], -1 ) );
            store.impose( new ElementInteger(h_Number[i], costArray, h_Cost[i], -1 ) );
            // store.impose( new XeqY( v_Cost[i], iv_costArray[ v_Number[i] ] ));
            // store.impose( new XeqY( h_Cost[i], iv_costArray[ h_Number[i] ] ));
        }

        // impose the total numbers
        IntVar total = new IntVar(store, "total", 0,  n * n );

        store.impose( new Sum( v_Number, total));
        store.impose( new XeqC( total , n_residential ));

        // sum the costs off all units
        IntVar v_Sum = new IntVar(store, "v_Sum", point_min * n, point_max * n);
        IntVar h_Sum = new IntVar(store, "h_Sum", point_min * n, point_max * n);
        IntVar cost = new IntVar(store, "cost", 2 * (point_min * n), 2 * (point_max * n));
        IntVar inv_cost = new IntVar(store, "inv_cost", 2 * (point_min * n), 2 * (point_max * n));

        store.impose( new Sum( v_Cost, v_Sum));
        store.impose( new Sum( h_Cost, h_Sum));
        store.impose( new XplusYeqZ( h_Sum, v_Sum , cost));


        store.impose( new XplusYeqC( cost, inv_cost , 0));


        // store.impose( new Lex(grid));
        // store.impose( new Lex(grid, true ));
        // store.impose( new Lex(grid, false ));
        for (int i = 1; i < n - 1; i++) {
            store.impose( new LexOrder(grid[i], grid[i + 1]));
            store.impose( new LexOrder(getVertical( i, grid ), getVertical( i + 1, grid)));
        }





        Search<IntVar> search = new DepthFirstSearch<IntVar>();
        SelectChoicePoint<IntVar> select = new SimpleMatrixSelect<IntVar>(
        grid, new SmallestDomain<IntVar>(), new IndomainMin<IntVar>());

        // search.setSolutionListener(new PrintOutListener<IntVar>());
        search.labeling(store, select, inv_cost);

        try {
            bw.write("OUTPUT: \ncost = " + cost.min() + "\n");
            // bw.write("\n-------------------------------------------------------\n\n" );


            int mid = (n+1)/2;

            bw.write("\n" );
            bw.write("-----" );
            for (int i = 0; i < n; i ++){
                bw.write("----" );
            }
            bw.write("\n\n" );



            for (int i = 0; i < n; i ++){
                if (i == mid || i == mid-1){bw.write("\n");}
                bw.write("\t");
                for (int j = 0; j < n; j ++){


                    if (j == mid || j == mid-1){bw.write("\t");}
                    bw.write( grid[i][j].min() + "\t");
                }
                bw.write("\n");
            }




            bw.write("\n" );
            bw.write("-----" );
            for (int i = 0; i < n; i ++){
                bw.write("----" );
            }
            bw.write("\n\n" );
            // bw.write("\n-------------------------------------------------------\n" );
        } catch (IOException ex) {
            ex.printStackTrace();
        }


        // return 1;
        return (cost.value());





    }
    public static IntVar[] getVertical( int j, IntVar[][] grid) {
        IntVar[] vert = new IntVar[grid.length];
        for (int i = 0; i < grid.length; i++) {
            vert[i] = grid[i][j];
        }
        return vert;
    }


}
